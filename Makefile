CFLAGS=-O2 -g -Wall -std=c++11 -pthread
CC=gcc
LDFLAGS=
CPP=g++

all: 
	$(MAKE) -C CUDA
	$(MAKE) -C OPENMP
	$(MAKE) -C Test
# Regula simpla de compilare
# Adaugati dupa nevoie CFLAGS, alte surse, biblioteci, etc.
# Puteti folosi gcc si sa schimbati extensia fisierului in .c, daca vreti sa folositi pthread. etc.
	

clean:
	$(MAKE) -C CUDA clean
	$(MAKE) -C OPENMP clean
	$(MAKE) -C Test clean
	rm -rf testdata/*2.bin testdata/ciphertext
