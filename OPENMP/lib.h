 /*
 *  lib.h
 *
 * Copyright (C) 2016 Adrian Matei <adi.matei@gmail.com> Casian Ciobanu <ciobanu.casian94@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
 
#ifndef LIB_H_
#define LIB_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <fstream>
#include <iostream>
#include <omp.h>

#endif /* LIB_H_ */