 /*
 *  helper.h
 *
 * Copyright (C) 2016 Adrian Matei <adi.matei@gmail.com> Casian Ciobanu <ciobanu.casian94@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
 
#ifndef HELPER_H_
#define HELPER_H_

#define MAX_FILENAME_SIZE 65536

#define TIME(func_call, time_pointer) \
		do { \
			double start = 0.0; \
			struct timespec t; \
			clock_gettime(CLOCK_REALTIME, &t); \
			start = t.tv_sec + t.tv_nsec / 1000000000.0; \
			func_call; \
			clock_gettime(CLOCK_REALTIME, &t); \
			*time_pointer += t.tv_sec + t.tv_nsec / 1000000000.0 - start; \
		} while (0)

int get_test_filenames(char ***test_filenames, const char *config_filename);

#endif /* HELPER_H_ */