 /*
 *  aes_parallel.cc
 *
 * Copyright (C) 2016 Adrian Matei <adi.matei@gmail.com> Casian Ciobanu <ciobanu.casian94@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
 
#include "helper.h"
#include "aes.h"
#include "lib.h"

using namespace std;



// The array that stores the round keys.
static uint8_t roundKey[176];
// The array that holds the plaintext for the current block.
uint8_t plaintext_block[BLOCKSIZE];
// The array that stores the ciphertext for the current block.
uint8_t ciphertext_block[BLOCKSIZE];

// 128bit key
uint8_t key[16] = { (uint8_t)0x2b, (uint8_t)0x7e, (uint8_t)0x15, (uint8_t)0x16,
					(uint8_t)0x28, (uint8_t)0xae, (uint8_t)0xd2, (uint8_t)0xa6,
					(uint8_t)0xab, (uint8_t)0xf7, (uint8_t)0x15, (uint8_t)0x88,
					(uint8_t)0x09, (uint8_t)0xcf, (uint8_t)0x4f, (uint8_t)0x3c };

bool silent = 0;

uint8_t seed[16] = { 0,5,1,0,0,0,6,0,24,0,0,0,0,0,0,0 };

void aes_ecb(const char *infile, const char *outfile, bool is_enc) {
	FILE *fp_in;
	FILE *fp_out;

	fp_in = fopen(infile, "rb");
	if (fp_in == NULL && !silent) {
		cout << "Can't open input file " << infile << endl;
		exit(EXIT_FAILURE);
	}
	fp_out = fopen(outfile, "wb+");
	if (fp_out == NULL && !silent) {
		// fprintf(stderr, "Can't open output file %s!\n", outfile);
		cout << "Can't open output file " << outfile << endl;
		exit(EXIT_FAILURE);
	}

	KeyExpansion(roundKey, key);

	// determine size of file, read file into plaintext and determine number of plaintext blocks
	fseek(fp_in, 0, SEEK_END);
	uintmax_t plaintext_size = ftell(fp_in);
	rewind(fp_in);
	uint8_t* plaintext = (uint8_t*)malloc(plaintext_size);
	uintmax_t bytes_read = fread(plaintext, sizeof(uint8_t), plaintext_size, fp_in);
	assert(bytes_read == plaintext_size);
	uintmax_t plaintext_blocks = (bytes_read + BLOCKSIZE - 1) / BLOCKSIZE;
	uint8_t* ciphertext = (uint8_t*)malloc(plaintext_blocks*BLOCKSIZE);

	if (!silent) {
		cout << "File size: " << plaintext_size << " bytes" << endl;
		cout << "Number of plaintext blocks: " << plaintext_blocks << " (blocksize: " << BLOCKSIZE << ") bytes" << endl;
	}

	uintmax_t j;
	int nr_threads = omp_get_max_threads();
	#pragma omp parallel for ordered num_threads(nr_threads) shared(plaintext, ciphertext, roundKey)
	for (j = 0; j < plaintext_size; j += BLOCKSIZE) {
		// encrypt plaintext block
		if (is_enc)
			#pragma omp ordered
			AES128_ECB_encrypt(plaintext + j, roundKey, ciphertext + j);
		else 
			#pragma omp ordered		
			AES128_ECB_decrypt(plaintext + j, roundKey, ciphertext + j);
	}

	// write ciphertext to output file
	fwrite(ciphertext, sizeof(uint8_t), BLOCKSIZE * plaintext_blocks, fp_out);

	fclose(fp_in);
	fclose(fp_out);
}

void aes_ctr(const char *infile, const char *outfile) {
	FILE *fp_in;
	FILE *fp_out;

	fp_in = fopen(infile, "rb");
	if (fp_in == NULL && !silent) {
		cout << "Can't open input file " << infile << endl;
		exit(EXIT_FAILURE);
	}
	fp_out = fopen(outfile, "wb+");
	if (fp_out == NULL && !silent) {
		// fprintf(stderr, "Can't open output file %s!\n", outfile);
		cout << "Can't open output file " << outfile << endl;
		exit(EXIT_FAILURE);
	}

	KeyExpansion(roundKey, key);

	// determine size of file, read file into plaintext and determine number of plaintext blocks
	fseek(fp_in, 0, SEEK_END);
	uintmax_t plaintext_size = ftell(fp_in);
	rewind(fp_in);
	uint8_t* plaintext = (uint8_t*)malloc(plaintext_size);
	uintmax_t bytes_read = fread(plaintext, sizeof(uint8_t), plaintext_size, fp_in);
	assert(bytes_read == plaintext_size);
	uintmax_t plaintext_blocks = (bytes_read + BLOCKSIZE - 1) / BLOCKSIZE;
	uint8_t* ciphertext = (uint8_t*)malloc(plaintext_blocks*BLOCKSIZE);

	if (!silent) {
		cout << "File size: " << plaintext_size << " bytes" << endl;
		cout << "Number of plaintext blocks: " << plaintext_blocks << " (blocksize: " << BLOCKSIZE << ") bytes" << endl;
	}

	uintmax_t i;
	int nr_threads = omp_get_max_threads();
	#pragma omp parallel num_threads(nr_threads) shared(plaintext, ciphertext, roundKey, plaintext_blocks)
	{
		#pragma omp single
		for (i = 0; i < plaintext_blocks; i++) {
			uint8_t inp[16];
			memcpy(inp, seed, 16);
			inp[0] += i;
			#pragma omp task
				AES128_ECB_encrypt(inp, roundKey, ciphertext_block);

			#pragma omp taskwait
			for (int j = 0; j < 16; ++j) {
				ciphertext[16 * i + j] = plaintext[16 * i + j] ^ ciphertext_block[j];
			}
			
			// encrypt plaintext block

			// write ciphertext block to output file
		}
	}

	// write ciphertext to output file
	fwrite(ciphertext, sizeof(uint8_t), BLOCKSIZE * plaintext_blocks, fp_out);

	fclose(fp_in);
	fclose(fp_out);

	if (!silent)
		cout << "Encryption of " << plaintext_blocks << " plaintext blocks successful!\n" << endl;

}

int main(int argc, char **argv) {
	if (argc < 5 || argc > 6) {
		cout << "Usage: aes_openmp encrypt/decrypt ECB/CTR <input file> <output file>" << endl;
		exit(EXIT_FAILURE);
	}

	if (argc == 6 && !strcmp(argv[5], "--silent"))
		silent = 1;

	double time_aes = 0;
	if (strcmp(argv[1], "encrypt") == 0) { 
		if (strcmp(argv[2], "CTR") == 0) 
			TIME(aes_ctr(argv[3], argv[4]), &time_aes);
		else if (strcmp(argv[2], "ECB") == 0) 
			TIME(aes_ecb(argv[3], argv[4], true), &time_aes);
	}
	else if (strcmp(argv[1], "decrypt") == 0) {
		if (strcmp(argv[2], "CTR") == 0) 
			TIME(aes_ctr(argv[3], argv[4]), &time_aes);
		else if (strcmp(argv[2], "ECB") == 0) 
			TIME(aes_ecb(argv[3], argv[4], false), &time_aes);
	}
	else {
		cout << "You didn't provide a valid first argument: encrypt/decrypt!" << endl;
		exit(EXIT_FAILURE);
	}
	cout << argv[1] << " AES-" << argv[2] << " for file " << argv[3] 
		 << " took: " << time_aes << " seconds" << endl << endl;
	return 0;
}
