 /*
 *  aes.h
 *
 * Copyright (C) 2016 Adrian Matei <adi.matei@gmail.com> Casian Ciobanu <ciobanu.casian94@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef AES_H_
#define AES_H_

#include <stdint.h>
#include <time.h>

// blocksize in bytes
#define BLOCKSIZE 16
// The number of columns comprising a state in AES.
#define LANESIZE 4
// The number of 32 bit words in a key.
#define KEYWORDS 4
// Key length in bytes [128 bit]
#define KEYLENGTH 16
// The number of rounds in AES Cipher.
#define ROUNDS 10

void KeyExpansion(uint8_t* roundKey, uint8_t* key);

void AES128_ECB_encrypt(uint8_t* input, const uint8_t* key, uint8_t *output);

void AES128_ECB_decrypt(uint8_t* input, const uint8_t* roundKey, uint8_t *output);

#endif /* AES_H_ */