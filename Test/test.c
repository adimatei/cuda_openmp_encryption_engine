 /*
 *  test.c
 *
 * Copyright (C) 2016 Adrian Matei <adi.matei@gmail.com> Casian Ciobanu <ciobanu.casian94@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

int launch_test_openmp(char * path, const char* plaintext, const char* ciphertext, const char* decryptedtext, char* mode);
int launch_test_cuda(char * path, const char* plaintext, const char* ciphertext, const char* decryptedtext, char* mode);



#define NUMBER_OF_FILES 7

const char* files[] = {
	" ../testdata/tmp100KB.bin ",
	" ../testdata/tmp500KB.bin ",
	" ../testdata/tmp6MB.bin ",
	" ../testdata/tmp17MB.bin ",
	" ../testdata/tmp57MB.bin ",
	" ../testdata/tmp120MB.bin ",
	" ../testdata/tmp209MB.bin ",

};

const char* files_decrypted[] = {
	" ../testdata/tmp100KB2.bin ",
	" ../testdata/tmp500KB2.bin ",
	" ../testdata/tmp6MB2.bin ",
	" ../testdata/tmp17MB2.bin ",
	" ../testdata/tmp57MB2.bin ",
	" ../testdata/tmp120MB2.bin ",
	" ../testdata/tmp209MB2.bin ",

};


void print_usage(){
	printf("Usage: test openmp/cuda ECB/CTR\n");
}


int main(int argc, char *argv[]) {
	if (argc != 3)
	{
		print_usage();
		return 1;
	}

	char PATH_PARALLEL[100];
	char PATH_OPENMP[100];


	strcpy(PATH_PARALLEL, "../CUDA/aes_cuda ");
	strcpy(PATH_OPENMP, "../OPENMP/aes_openmp ");


	const char* ciphertext = " ../testdata/ciphertext ";
	
	int i;
	for (i = 0; i < NUMBER_OF_FILES; i++) {
		printf("> Test for file %s\n", files[i]);
		if(strcmp(argv[1], "openmp") == 0) {
			launch_test_openmp(PATH_OPENMP, files[i], ciphertext, files_decrypted[i], argv[2]);
		} else if(strcmp(argv[1], "cuda") == 0){
			launch_test_cuda(PATH_PARALLEL, files[i], ciphertext, files_decrypted[i], argv[2]);
		} else {
			print_usage();
			return 1;
		}
	}

	printf("Press any key to return...\n");
	getchar();

	return 0;
}

int launch_test_openmp(char * path, const char* plaintext, const char* ciphertext, const char* decryptedtext, char* mode) {
	char command[200];

	//// prepare command for OpenMP encryption
	strcpy(command, path);
	strcat(strcat(strcat(strcat(strcat(command, " encrypt "), mode), plaintext), ciphertext), " --silent ");
	
	// execute OpenMP encryption
	printf("=================== AES-%s encrypt OpenMP ===================\n", mode);
	if (system(command))
		exit(1);

	// prepare command for OpenMP decryption
	strcpy(command, path);
	strcat(strcat(strcat(strcat(strcat(command, " decrypt "), mode), ciphertext), decryptedtext), "");

	// execute OpenMP decryption
	printf("\n================== AES-%s decrypt OpenMP ==================\n", mode);
	if (system(command))
		exit(1);
	printf("==================================================\n\n\n");
	return 0;
}

int launch_test_cuda(char * path, const char* plaintext, const char* ciphertext, const char* decryptedtext, char* mode) {
	char command[200];

	//// prepare command for CUDA encryption
	strcpy(command, path);
	strcat(strcat(strcat(strcat(strcat(command, " encrypt "), mode), plaintext), ciphertext), "");

	// execute CUDA encryption
	printf("=================== AES-%s encrypt CUDA ===================\n", mode);
	if (system(command))
		exit(1);

	// prepare command for CUDA decryption
	strcpy(command, path);
	strcat(strcat(strcat(strcat(strcat(command, " decrypt "), mode), ciphertext), decryptedtext), "");

	// execute CUDA decryption
	printf("\n================== AES-%s decrypt CUDA ==================\n", mode);
	if (system(command))
		exit(1);
	printf("==================================================\n\n\n");
	return 0;

}
