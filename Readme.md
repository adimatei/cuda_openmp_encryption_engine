### AES - CUDA si OpenMP
Implementare minimala a algoritmului AES-128 in tehnologii de paralelizare pe CPU si GPU.

## Introducere
Criptarea a devenit cea mai populara metoda de protectie, atat pentru comunicatii, cat si pentru datele cu caracter secret. Pe masura constientizarii beneficiilor aduse de utilizarea criptarii, a dezavantajelor lipsei de protectie a informatiilor si a faptului ca tehnologia de criptare a devenit mai simpla, mai accesibila, criptarea devine o metoda atractiva de protejare a datelor.

## Algoritmul AES (Advanced Encryption Standard)
AES a devenit succesorul oficial al standardului DES incepand cu sfarsitul anului 2001. Desi specificatia AES standardizeaza dimensiunile de 128, 192 si 256 de biti pentru lungimea cheii, lungimea blocului este restrictionata la 128 de biti. Astfel, intrarea si iesirea algoritmului de criptare/decriptare este un bloc de 128 de biti. la inceputul criptarii/decriptarii, blocul este copiat intr-un tablou denumit stare (_state_), primii patru octeti pe prima coloana, apoi urmatorii patru pe a doua coloana si tot asa pana la completarea tabloului.

## Modul ECB (Electronic Code Book)
Textul clar se imparte in blocuri de lungime n, unde n are valoarea de 128 de biti. Ultimul bloc se completeaza (_padding_) la lungimea n; bitii adaugati pot fi zerouri, biti aleatori sau se pot utiliza alte scheme de completare (**PKCS #1, PKCS #5**). In final, fiecare bloc se cripteaza independent de celelalte.

## Modul CTR (Counter)
Se genereaza un sir pseudoaleator (numit _nonce_), de lungime 64 de biti la care se concateaza un numar (_counter_), de asemeanea pe 64 de biti, care este incrementat per fiecare iteratie (**c, c + 1, c + 2, ..., c + n - 1**). Apoi, valoarea obtinuta (_nonce || counter_) este criptata cu cheia de sesiune. Se obtine un stream pseudoaleator de lungime 128 de biti, iar in final, se face XOR cu fiecare bit din bloc.

## OpenMP 
**"OpenMP is an Application Program Interface (API) that may be used to explicitly direct multithreaded, shared memory parallelism"**. Acest API ofera facilitati mai usoare pentru a putea profita de prezenta mai multor _core-uri_ din procesor, unde un thread principal creaza un numar de thread-uri pentru a imparti munca. In princpiu, fiecare thread executa in paralel si independent sectiunea de cod aferenta lui. Logica modelului de programare paralela se implementeaza prin directive de compilator:
```
#pragma omp parallel for
```
Paralelizarea programului facut de noi, folosind OpenMP, a fost destul de banala. Se observa ca bucla care proceseaza fiecare bloc din _plaintext_ se poate paraleliza folosind directive OpenMP. 

## CUDA (Compute Unified Device Architecture)
CUDA este un sistem hardware si software folosit pentru a profita de puterea de procesare a placilor grafice **NVIDIA**. Placile grafice sunt foarte puternice atunci cand vine vorba despre paralelismul datelor, avand in vedere faptul ca au mai multe unitati aritmetice (_ALU_) si un debit mare in a paraleliza _task-uri_. Un lucru important de specificat este ca GPU este un coprocesor cu un spatiu de memorie independent. De aceea, este necesar sa copiem datele din memoria principala (_memoria CPU_) in memoria GPU. Codul este executat in functii de pe GPU numite _kernels_. Atunci cand apelam o functie _kernel_ trebuie sa ii specificam numarul de blocuri si dimensiunea blocului. Dimensiunea blocului este reprezentata de numarul de thread-uri care vor fi create per microprocesor.
In cazul implementarii CUDA, _plaintext-ul_ este copiat in memoria GPU, unde este procesat, ulterior fiind copiat inapoi in memoria CPU si scris intr-un fisier de iesire. Pentru a obtine numarul de blocuri si dimensiunea unu bloc se foloseste functia **cudaOccupancyMaxPotentialBlockSize** care returneaza numarul minim de blocuri si dimensiunea blocului, necesare pentru obtine cel mai bun rezultat.


## Mod de utilizare
In directorul radacina se da comanda _make_ sau _make -f Makefile_. Aceasta creaza executabilele, atat pentru cele doua implementari (cu OpenMP si CUDA), cat si pentru executabilul de test. Pentru a testa aplicatiile, se intra in directorul _Test_ si ruleaza **./test openmp/cuda ECB/CTR**:
```
./test openmp ECB
```
Primul argument    - testarea aplicatiei implementata cu OpenMP sau CUDA. 
Al doilea argument - modul de criptare/decriptare ales pentru AES.
Se testeaza atat criptarea, cat si decriptarea pentru 7 fisiere de dimensiunea variabila, care se gasesc in directorul _testdata_.

Daca se doreste testarea individuala a aplicatiilor se intra in directorul dorit (CUDA sau OPENMP), se da comanda _make_ sau _make -f Makefile_. In urma rularii acestei comenzii se obtin executabilele dorite.
Pentru testarea aplicatiei implentata cu OpenMP:
```
./aes_openmp encrypt/decrypt ECB/CTR <input file> <output file> [--silent]
```

Pentru testarea aplicatiei implemetata cu CUDA:
```
./aes_cuda encrypt/decrypt ECB/CTR <input file> <output file> [--silent]
```
primul argument      - criptare sau decriptare.
al doilea argument   - modul de criptare/decriptare ales pentru AES.
al treilea argument  - fisierul care va fi criptat/decriptat.
al patrulea argument - fisierul care va fi decriptat/criptat.
--silent             - daca se doreste ca niciun mesaj sa fie afisat pe ecran (_verbose_).

Fisierele de folosite pentru testare au urmatoarele dimensiuni:

|  Nume fisier 	| Dimensiune in octeti 	|
|:------------:	|:--------------------:	|
| tmp100KB.bin 	|        102,400       	|
| tmp500KB.bin 	|        512,000       	|
|  tmp6MB.bin  	|       6,291,456      	|
|  tmp17MB.bin 	|      17,825,792      	|
|  tmp57MB.bin 	|      59,768,832      	|
| tmp120MB.bin 	|      125,829,120     	|
| tmp209MB.bin 	|      219,152,384     	|


Specificatiile platformei de utilizare:


| Numele specificatiei tehnice | Valoarea specificatiei tehnice |
|:----------------------------:|:------------------------------:|
|     Tipul micropresorului    |  Intel Core i7 6700HQ 2,6 GHz  |
|          Moduri CPU          |             OpenMP             |
|             RAM              |              8 GB              |
|              GPU             |     Nvidia GeForce GTX 960M    |
|              OS              |          Ubuntu 16.04          |

## Rezultatele testelor
In ceea ce priveste rezultatele testelor, acestea nu reprezinta o surpriza, ele fiind in concordanta cu cele obtinute in alte studii de specialitate. In urmatorul tabel, sunt reprezentate rezultatele testelor pentru criptare/decriptare folosind OpenMP, atat in modul AES-ECB, cat si in modul AES-CTR.

_OpenMP_| | | | | | 
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
Dimensiunile fișierelor folosite pentru test| |Parametri de performanță testați|ECB| |CTR|  
 | | |Criptare|Decriptare|Criptare|Decriptare
100 KB| |TIME (Sec)|0.064|0.066|0.041|0.022
 | |PERF(MB/Sec)|1.534|1.469|2.409|4.397
500 KB| |TIME (Sec)|0.070|0.232|0.108|0.105
 | |PERF(MB/Sec)|6.980|2.109|4.503|4.635
6 MB| |TIME (Sec)|0.766|2.736|1.286|1.285
 | |PERF(MB/Sec)|7.830|2.193|4.666|4.671
17 MB| |TIME (Sec)|2.093|7.752|3.655|3.630
 | |PERF(MB/Sec)|8.121|2.193|4.651|4.684
57 MB| |TIME (Sec)|6.999|25.863|12.101|12.812
 | |PERF(MB/Sec)|8.144|2.204|4.710|4.449
120 MB| |TIME (Sec)|14.720|54.214|26.333|25.671
 | |PERF(MB/Sec)|8.152|2.213|4.557|4.674
209 MB| |TIME (Sec)|27.387|101.451|47.329|44.975
 | |PERF(MB/Sec)|7.631|2.060|4.416|4.647
**Concluzie**| |**Median PERF (MB/Sec)**|**6.913**|**2.063**|**4.273**|**4.594**


In tabelul urmator se prezinta rezultatele pentru criptarea/decriptarea folosind CUDA, pentru aceleași moduri de operare AES.

_CUDA_| | | | | | 
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
Dimensiunile fișierelor folosite pentru test| |Parametri de performanță testați|AES-ECB| |AES-CTR| 
 | | |Criptare|Decriptare|Criptare|Decriptare
100 KB| |TIME (Sec)|0.0001|0.0008|0.0001|0.0001
 | |PERF(MB/Sec)|813.1516|123.0845|1,243.5851|1,293.1177
500 KB| |TIME (Sec)|0.0004|0.0029|0.0003|0.0002
 | |PERF(MB/Sec)|1,239.3428|170.7048|1,889.1654|2,084.5340
6 MB| |TIME (Sec)|0.0042|0.0333|0.0024|0.0024
 | |PERF(MB/Sec)|1,428.0057|179.9652|2,449.6035|2,460.7914
17 MB| |TIME (Sec)|0.0128|0.0930|0.0088|0.0069
 | |PERF(MB/Sec)|1,328.6830|182.7143|1,924.6236|2,452.6891
57 MB| |TIME (Sec)|0.0417|0.2950|0.0266|0.0273
 | |PERF(MB/Sec)|1,367.0355|193.2341|2,145.7484|2,089.2124
120 MB| |TIME (Sec)|0.0766|0.5836|0.0550|0.0560
 | |PERF(MB/Sec)|1,566.0118|205.6324|2,181.7674|2,142.7054
209 MB| |TIME (Sec)|0.1316|1.0143|0.0958|0.0866
 | |PERF(MB/Sec)|1,588.7245|206.0541|2,182.1481|2,412.2015
**Concluzie**| |**Median PERF (MB/Sec)**|**1,332.9936**|**180.1985**|**2,002.3774**|**2,133.6074**

Creșterea de viteză medie a implementării CUDA față de implementarea OpenMP

|       Algoritm       | Crestere de performanta |
|:--------------------:|:-----------------------:|
|  AES-ECB - Criptare  |       x192.8152402      |
| AES-ECB - Decriptare |       x87.35100057      |
|  AES-CRT - Criptare  |       x468.6086252      |
| AES-CRT - Decriptare |       x464.4448326      |

## Concluzie
Tehnologiile de paralelizare folosind GPU au evoluat foarte mult în ultimii ani, acest aspect aflandu-se in concordanta cu rezultatele testelor noastre. Se poate observa potentialul urias al paralelizarii algoritmilor criptografici folosind GPU, ceea ce poate fi util intr-o larga varietate de domenii de cercetare si dezvoltare de sisteme de securitate software/hardware. Dezvoltarea continua a acestora ne va permite sa fim cu un pas mai aproape de accesul securizat la Internet, la informatie.

## Realizatori
1. Adrian Matei
2. Casian Ciobanu

## Bibliografie
 1. https://computing.llnl.gov/tutorials/openMP/#Introduction 
 2. http://bisqwit.iki.fi/story/howto/openmp/
 3. http://www.nvidia.com/docs/IO/116711/sc11-cuda-c-basics.pdf
 4. http://supercomputingblog.com/cuda-tutorials/
 5. https://github.com/mxwlone/aes-cuda